<?php
namespace Tests\Feature;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /** @test */
    public function it_can_retrieve_users()
    {
        $users = factory('App\User', 3)->create();

        $this->get('api/user')
            ->seeJsonContains([
                'success' => true,
                'message' => 'User successfully retrieved',
            ])
            ->seeJsonStructure([
                'data' => [['id', 'name']],
            ])
            ->seeStatusCode(200)
            ->assertResponseOk();

        foreach ($users->toArray() as $user) {
            $this->seeInDatabase('users', $user);
        }
    }

    /** @test */
    public function it_can_retrieve_a_user()
    {

        $user = factory('App\User')->create();

        $this->get("api/user/{$user->id}")
            ->seeJsonContains([
                'success' => true,
                'message' => 'User successfully retrieved',
            ])
            ->seeJsonStructure([
                'data' => ['id', 'name'],
            ])
            ->seeStatusCode(200)
            ->assertResponseOk();

        $this->seeInDatabase('users', $user->toArray());
    }

    /** @test */
    public function it_can_create_a_user()
    {
        $user = factory('App\User')->make();

        $this->post('api/user', $user->toArray())
            ->seeJsonContains([
                'success' => true,
                'message' => 'User successfully created',
            ])
            ->seeJsonStructure(['data' => ['id', 'name']])
            ->seeStatusCode(200)
            ->assertResponseOk();

        $this->seeInDatabase('users', $user->toArray());
    }

    /** @test */
    public function it_can_delete_a_user()
    {
        $user = factory('App\User')->create();

        $this->seeInDatabase('users', $user->toArray());

        $this->delete("api/user/{$user->id}")
            ->seeJsonEquals([
                'success' => true,
                'message' => 'User successfully deleted',
            ])
            ->seeStatusCode(200)
            ->assertResponseOk();

        $this->notSeeInDatabase('users', $user->toArray());
    }

}

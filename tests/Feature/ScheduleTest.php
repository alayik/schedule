<?php
namespace Tests\Feature;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\TestCase;

class ScheduleTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /** @test */
    public function it_can_retrieve_user_schedules_in_month()
    {
        $user = factory('App\User')->create();
        $schedules = factory('App\Schedule', 50)->create(['user_id' => $user->id]);

        $this->get("api/schedule/{$user->id}/month/2020-01")
            ->seeJsonContains([
                'success' => true,
                'message' => 'schedules successfully retrieved',
            ])
            ->seeStatusCode(200)
            ->assertResponseOk();

        foreach ($schedules->toArray() as $schedule) {
            $this->seeInDatabase('schedules', $schedule);
        }
    }

    /** @test */
    public function it_can_create_a_schedule()
    {
        $schedule = factory('App\Schedule')->make();
        $user = factory('App\User')->create();

        $this->post('api/schedule', array_merge($schedule->toArray(), ['user_id' => $user->id]))
            ->seeJsonContains([
                'success' => true,
                'message' => 'schedule successfully created',

            ])
            ->seeJsonStructure(['data' => ['id', 'title', 'description', 'date_todo', 'time_todo', 'done']])
            ->seeStatusCode(200)
            ->assertResponseOk();

        $this->seeInDatabase('schedules', $schedule->toArray());
    }

    /** @test */
    public function it_can_update_a_schedule()
    {
        $schedule = factory('App\Schedule')->states('user')->create();
        $schedule_change = factory('App\Schedule')->make();

        $this->seeInDatabase('schedules', $schedule->toArray());

        // not exist schedule
        $this->put("api/schedule/5", array_merge($schedule->toArray(), ['title' => 'changed']))
            ->seeJsonEquals([
                'success' => false,
                'error' => true,
                'schedule' => 'schedule not found',
            ])
            ->seeStatusCode(404);

        $this->put("api/schedule/{$schedule->id}", $schedule_change->toArray())
            ->seeJsonContains([
                'success' => true,
                'message' => 'schedule successfully updated',
            ])
            ->seeJsonStructure(['data' => ['id', 'title', 'description', 'date_todo', 'time_todo', 'done']])
            ->seeStatusCode(200)
            ->assertResponseOk();

        $this->seeInDatabase('schedules', $schedule_change->toArray());
    }

    /** @test */
    public function it_can_done_a_schedule()
    {
        $schedule = factory('App\Schedule')->states('user')->create(['done' => false]);

        $this->seeInDatabase('schedules', $schedule->toArray());

        // not exist schedule
        $this->put("api/schedule/5", array_merge($schedule->toArray(), ['title' => 'changed']))
            ->seeJsonEquals([
                'success' => false,
                'error' => true,
                'schedule' => 'schedule not found',
            ])
            ->seeStatusCode(404);

        $this->put("api/schedule/{$schedule->id}/done")
            ->seeJsonContains([
                'success' => true,
                'message' => 'schedule successfully done',
            ])
            ->seeStatusCode(200)
            ->assertResponseOk();

        $this->seeInDatabase('schedules', ['id' => $schedule->id, 'done' => true]);
        $this->notSeeInDatabase('schedules', ['id' => $schedule->id, 'done' => false]);
    }

    /** @test */
    public function it_can_delete_a_schedule()
    {
        $schedule = factory('App\Schedule')->states('user')->create();

        $this->seeInDatabase('schedules', $schedule->toArray());

        $this->delete("api/schedule/{$schedule->id}")
            ->seeJsonEquals([
                'success' => true,
                'message' => 'schedule successfully deleted',
            ])
            ->seeStatusCode(200)
            ->assertResponseOk();

        $this->notSeeInDatabase('schedules', $schedule->toArray());
    }

}

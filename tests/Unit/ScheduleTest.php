<?php
namespace Tests\Unit;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Schema;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\TestCase;

class ScheduleTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    protected $schedule;

    public function setUp(): void
    {
        parent::setUp();

        $this->schedule = factory('App\Schedule')->states('user')->create()->first();
    }

    /** @test  */
    public function it_can_check_schedule_table_columns_in_the_database()
    {
        $this->assertTrue(
            Schema::hasColumns('schedules', [
                'user_id', 'title', 'description', 'date_todo', 'time_todo', 'done', 'created_at', 'updated_at',
            ]), 1);
    }

    /** @test */
    public function scheduleـisـrelatedـtoـuser()
    {
        $schedule = new \App\Schedule;

        // Get the relationship object
        $user = $schedule->user();
        $related_user = $user->getRelated();

        // Check the relationship
        $this->assertInstanceOf(BelongsTo::class, $user);
        $this->assertInstanceOf(\App\User::class, $related_user);
    }

    /** @test */
    public function it_can_create_a_schedule()
    {
        // check the schedule
        $this->seeInDatabase('schedules', $this->schedule->toArray());

        // check the schedule owner
        $this->seeInDatabase('users', ['id' => $this->schedule->user_id]);

    }
}

<?php
namespace Tests\Unit;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory('App\User')->create()->first();
    }

    /** @test  */
    public function it_can_check_user_table_columns_in_the_database()
    {
        $this->assertTrue(
            Schema::hasColumns('users', [
                'name', 'created_at', 'updated_at',
            ]), 1);
    }

    /** @test */
    public function userـisـrelatedـtoـschedule()
    {
        $user = new \App\User;
        // Get the relationship object
        $schedules = $user->schedules();
        $related_schedules = $schedules->getRelated();

        // Check relationship
        $this->assertInstanceOf(HasMany::class, $schedules);
        $this->assertInstanceOf(\App\Schedule::class, $related_schedules);
    }

    /** @test */
    public function it_can_create_a_user_and_user_schedules()
    {
        $this->user->schedules()->createMany(
            factory('App\Schedule', 3)->make()->toArray()
        );

        // check user
        $this->seeInDatabase('users', $this->user->toArray());

        // check schedule
        $this->assertCount(3, $this->user->schedules->toArray());
        $this->seeInDatabase('schedules', $this->user->schedules->first()->toArray());

    }

}

const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

mix
.js('resources/js/app.js', 'public/assets/js')
.sass('resources/scss/app.scss', 'public/assets/css')
.options({
		processCssUrls: false,
		postCss: [tailwindcss('tailwind.config.js')]
});

mix.copyDirectory('resources/img','public/assets/img')

if (mix.inProduction()) {
	// somethimg...
}


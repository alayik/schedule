<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */

$router->get('/', [
    'as' => 'index',
    'uses' => 'UserController@index',
]);

$router->get('/user/{id}/schedule', [
    'as' => 'user.schedule',
    'uses' => 'UserController@schedule',
]);

$router->get('/routes', function () {
    dd(app('router')->getRoutes());
});

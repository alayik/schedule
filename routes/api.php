<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application.
|
 */

// user routes
$router->get('user[/[{id}]]', 'UserController@retrieve');
$router->post('user', 'UserController@create');
$router->delete('user/{id}', 'UserController@delete');

// schedule routes

$router->group([
    'prefix' => 'schedule',
], function ($router) {
    $router->get('{user}/month/{date}', 'ScheduleController@retrieveInMonth');
    $router->post('/', 'ScheduleController@create');
    $router->put('{id}', 'ScheduleController@update');
    $router->put('{id}/done', 'ScheduleController@done');
    $router->delete('{id}', 'ScheduleController@delete');
});

/*
|--------------------------------------------------------------------------
| Application Components
|--------------------------------------------------------------------------
|
|	Here is where you can register all of the components for an application.
|
*/

import Users from "./users"
import { Datetime } from 'vue-datetime';
import Modal from './modal';
import { TaskForm, TaskCalendar, ChooseDate } from './schedules';




let list = [
    Users,
    Datetime,Modal,
    TaskForm, TaskCalendar, ChooseDate
];

list.map(component => {
    Vue.component(component.name, component)
});

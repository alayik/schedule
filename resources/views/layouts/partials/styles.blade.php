<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700"  type="text/css" rel="stylesheet">

<!-- Favicon -->
<link rel="icon" href="{{ app('url')->asset('assets/img/time-and-date-icon.png') }}" type="image/x-icon"/>

<!-- App CSS -->
<link  href="{{ app('url')->asset('assets/css/app.css') }}"  type="text/css" rel="stylesheet">

@stack('style')

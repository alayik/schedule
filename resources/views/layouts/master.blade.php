<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        @include('layouts.partials.meta')
        @include('layouts.partials.styles')
        @yield('head')
    </head>
    <body class="tw-overflow-x-hidden">
        <div id="app">
            @include('layouts.sections.header')
            @yield('content')
            @include('layouts.sections.footer')
        </div>
        @include('layouts.partials.scripts')
    </body>
</html>

<header id="header">
    <nav class="navbar tw-w-screen tw-flex tw-items-center tw-justify-center tw-flex-wrap tw-p-2 tw-mb-2 tw-text-white tw-text-xs tw-z-50 md:tw-justify-start">
    	<a
            href="{{ route('index') }}"
    		class="tw-flex tw-items-center tw-justify-center tw-content-center tw-rounded tw-px-3 tw-py-2 tw-bg-blue tw-z-50 tw-capitalize tw-shadow all-duration-1
    		{{ app('url')->current() == route('index') ? 'active':'' }}"
    	>Users</a>
    </nav>
</header>

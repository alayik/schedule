@extends('layouts.master')

@section('title','404')

@section('content')
	<h2 class="tw-text-2xl tw-text-center tw-m-2 tw-mb-8 tw-text-dark tw-font-bold tw-uppercase">
		<span class="tw-border-b tw-border-dashed tw-border-grey">not found page</span>
	</h2>
@endsection

@extends('layouts.master')

@section('title',"Schedule | {$user->name }")

@section('content')
  	<section class="head tw-mb-8">
        <h2 class="tw-text-2xl tw-text-center tw-m-2 tw-text-dark tw-font-bold tw-uppercase">
            <span class="tw-border-b tw-border-dashed tw-border-grey">'{{ $user->name }}' Schedules</span>
        </h2>
    </section>
    <div class="tw-flex tw-flex-wrap tw-p-2 tw-relative">
		<section class="tw-w-full sm:tw-w-2/3 md:tw-w-3/4 tw-px-2 tw-mb-8">

			<div class="head tw-mb-4">
	            <h2 class="tw-text-lg tw-text-center tw-m-2 tw-text-dark tw-font-bold tw-uppercase">
	                <span class="tw-border-b tw-border-dashed tw-border-grey">calendar tasks</span>
	            </h2>
	        </div>

	        <choose-date></choose-date>

			<task-calendar :user-id="{{ $user->id }}"></task-calendar>

		</section>

		<aside class="tw-w-full sm:tw-w-1/3 md:tw-w-1/4 tw-px-2 tw-mb-8 md:tw-fixed md:tw-right-0">
            <task-form :user-id="{{ $user->id }}"></task-form>
		</aside>
   </div>


@endsection

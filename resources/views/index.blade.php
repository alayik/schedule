@extends('layouts.master')

@section('title','Users')

@section('content')
 	<section class="head tw-mb-8">
		<h2 class="tw-text-2xl tw-text-center tw-m-2 tw-text-dark tw-font-bold tw-uppercase">
	        <span class="tw-border-b tw-border-dashed tw-border-grey">Users</span>
	    </h2>
	    <p class="tw-text-sm tw-text-center">first create a user then go to user's schedule</p>
 	</section>

	<users/>
@endsection

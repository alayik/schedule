<?php

namespace App\Http\Controllers\Api;

use App\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class ScheduleController extends BaseController
{
    /**
     * get user's schedules by date in it month
     *
     * @param  integer $user
     * @param  string $date  is valid : year-month or year-month-day
     *
     * @return json
     */
    public function retrieveInMonth($user, $date)
    {
        $date_params = explode('-', $date);
        $start_date = Carbon::create(...$date_params)->startOfMonth()->toDateString();
        $end_date = Carbon::create(...$date_params)->lastOfMonth()->toDateString();

        $schedules = Schedule::where('user_id', $user)->whereBetween('date_todo', [$start_date, $end_date])
            ->orderBy('time_todo', 'asc')
            ->get()
            ->groupBy('date_todo')
            ->sortKeys();

        return response()->json([
            'success' => true,
            'message' => 'schedules successfully retrieved',
            'data' => $schedules->toJson(),
        ]);
    }
    /**
     * create a schedule
     * @param  Illuminate\Http\Request $request
     *
     * @return json
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'title' => 'required',
            'description' => '',
            'date_todo' => 'required',
            'time_todo' => '',
        ]);

        $schedule = Schedule::create([
            'user_id' => $request->input('user_id'),
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'date_todo' => $request->input('date_todo'),
            'time_todo' => $request->input('time_todo'),
        ]);

        return response()->json([
            'success' => true,
            'message' => 'schedule successfully created',
            'data' => $schedule->refresh()->only(['id', 'title', 'description', 'date_todo', 'time_todo', 'done']),
        ]);

    }

    /**
     * update a user's schedule
     *
     * @param  Illuminate\Http\Request $request
     * @param  integer    $id   schedule id
     *
     * @return json
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'date_todo' => 'required',
            'time_todo' => 'required',
        ]);

        $schedule = Schedule::whereId($id)->first();

        if (is_null($schedule)) {
            return response([
                'success' => false,
                'error' => true,
                'schedule' => 'schedule not found',
            ], 404);
        }

        $schedule->fill($request->only(['title', 'description', 'date_todo', 'time_todo']))->save();

        return response()->json([
            'success' => true,
            'message' => 'schedule successfully updated',
            'data' => $schedule->refresh()->only(['id', 'title', 'description', 'date_todo', 'time_todo', 'done']),
        ]);
    }

    /**
     * the schedule is done status update to true state
     *
     * @param  integer    $id   schedule id
     *
     * @return json
     */
    public function done($id)
    {
        $schedule = Schedule::whereId($id)->first();

        if (is_null($schedule)) {
            return response([
                'success' => false,
                'error' => true,
                'schedule' => 'schedule not found',
            ], 404);
        }

        $schedule->done = true;
        $schedule->save();

        return response()->json([
            'success' => true,
            'message' => 'schedule successfully done',
        ]);
    }

    /**
     * delete a user's schedule
     * @param  integer    $id   schedule id
     *
     * @return json
     */
    public function delete($id)
    {
        Schedule::whereId($id)->delete();

        return response()->json([
            'success' => true,
            'message' => 'schedule successfully deleted',
        ]);
    }
}

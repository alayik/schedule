<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class UserController extends BaseController
{
    /**
     * get users or a user by id
     * @param  integer $id
     *
     * @return json
     */
    public function retrieve($id = null)
    {
        $user = User::select(['id', 'name']);
        $data = is_null($id) ? $user->get()->toArray() : $user->whereId($id)->first();

        return response()->json([
            'success' => true,
            'message' => 'User successfully retrieved',
            'data' => $data,
        ]);
    }

    /**
     * create a user
     * @param  Illuminate\Http\Request $request
     *
     * @return json
     */
    public function create(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
        ]);

        $user = User::create([
            'name' => $request->input('name'),
        ]);

        return response()->json([
            'success' => true,
            'message' => 'User successfully created',
            'data' => $user->only(['name', 'id']),
        ]);
    }

    /**
     * update a user
     * not define
     *
     * @param  Illuminate\Http\Request $request
     * @param  integer    $id
     *
     */
    public function update(Request $request, $id)
    {
        # code...
    }

    /**
     * delete a user
     * @param  integer   $id
     *
     * @return json
     */
    public function delete($id)
    {
        User::whereId($id)->delete();

        return response()->json([
            'success' => true,
            'message' => 'User successfully deleted',
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\User;

class UserController extends Controller
{

    /**
     * show users
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }

    public function schedule($id)
    {
        $user = User::findOrFail($id);

        return view('schedule', compact('user'));
    }
}

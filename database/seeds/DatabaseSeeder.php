<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');

        $user = factory('App\User')->create();

        factory('App\Schedule', 60)->create(['user_id' => $user->id]);

    }
}

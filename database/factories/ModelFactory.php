<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name(),
    ];
});

$factory->define(App\Schedule::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'description' => $faker->paragraph,
        'date_todo' => date_format($faker->dateTimeBetween($startDate = '-3 months'), 'Y-m-d'),
        'time_todo' => $faker->time('H:i'),
    ];
});
$factory->state(App\Schedule::class, 'user', function ($faker) {
    return [
        'user_id' => factory(App\User::class)->create()->id,
    ];
});
$factory->state(App\Schedule::class, 'done', function ($faker) {
    return [
        'done' => $faker->boolean(),
    ];
});

# Schedule

*A test project on the subject of the user's schedule in the calendar*

> Create a user first and then create, delete, and edit schedules in the calendar
> And watch the user's work day plans

**CREATED WITH :**
- Laravel Lumen
- Vue (No use of vue-router, vuex, etc)
- TailwindCss


## Getting Started

*Initial measures for project implementation*

**STEPS:**

- `composer install`
- `npm install`
- `npm run prod`
- Change .env.example to .env and set default amount
- `php artisan migrate` (If this has not been done before)
- `php artisan db:seed ` (if want to create fake data)
- `php -S localhost:8000 -t public` (to serve this project locally)

